<div align="center">

<p align="center">
  <a href="" rel="noopener">
    <img width="30%" src="./ansible-wordmark.png" alt="Ansible Wordmark"></a>
    <br>
    <img width="70%"src="./glances-logo.png" alt="Glances logo">
  </a>
</p>

  [![Status](https://img.shields.io/badge/status-Inactive-inactive.svg)]() 
  [![Ansible Galaxy](https://img.shields.io/ansible/role/TODO)]() 
  [![Ansible Version](https://img.shields.io/badge/ansible-TODO-informational)]() 
  [![Ansible Version](https://img.shields.io/ansible/quality/TODO)]() 
  [![License](https://img.shields.io/badge/license-GPLv3-blue.svg)](/LICENSE)

</div>

---

# Ansible App - Glances

Ansible role to install glances and monitor remotely.

- Installs Python-Pip
- Installs Glances via Pip3
- Creates Glances configuration folder
- Copies Glances Hashed Password File
- Copies Glances Systemd Service File
- Enable Glances Systemd Service
- Allows Glances Server Port

## 🦺 Requirements

- `ufw`

## 🗃️ Role Variable

*None*

## 📦 Dependencies

*None*

## 🤹 Example Playbook

*TODO*

## ⚖️ License

This code is released under the [GLPv3](./LICENSE) license. For more information refer to `LICENSE.md`

## ✍️ Author Information

Gerson Rojas <thegrojas@protonmail.com>
